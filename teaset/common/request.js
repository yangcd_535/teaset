// // 目前没有针对uni的Fly版本，使用wx版本没有问题
// #ifdef APP-PLUS
import Fly from 'flyio/dist/npm/wx'
// #endif
// #ifdef MP-WEIXIN
import Fly from 'flyio/dist/npm/wx'
// #endif
// #ifdef H5
import Fly from 'flyio/dist/npm/fly'
// #endif

const request = new Fly()

const errorPrompt = (err) => {
	// #ifdef MP-WEIXIN
	wx.showToast({
		title: err.message || 'fetch data error.',
		icon: 'none'
	})
	// #endif
	// #ifdef APP-PLUS
	uni.showToast({
		title: err.message || 'fetch data error.',
		icon: 'none'
	})
	// #endif
}

request.interceptors.request.use((request) => {
	// wx.showNavigationBarLoading()
	uni.showLoading({
		title: '加载中'
	});
	return request
})

request.interceptors.response.use((response, promise) => {
	// wx.hideNavigationBarLoading()
	uni.hideLoading();
	// if (!(response && response.data && response.data.res === 0)) {
	//   errorPrompt(response)
	// }
	return promise.resolve(response.data)
}, (err, promise) => {
	// wx.hideNavigationBarLoading()
	uni.hideLoading();

	errorPrompt(err)
	return promise.reject(err)
})

export default request
