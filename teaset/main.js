import Vue from 'vue'
import App from './App'

import store from './store'

Vue.config.productionTip = false
Vue.prototype.$API_BASE=' https://www.easy-mock.com/mock/5bd6be9efffb9b57ff8cf3a7/teaset';



import request from '@/common/request'

//常用组件，全局注册
import tsBadge from '@/components/teaset/components/ts-badge.vue';
import tsIcon from '@/components/teaset/components/ts-icon.vue';
import tsTag from '@/components/teaset/components/ts-tag.vue';
import tsLoadMore from '@/components/teaset/components/ts-load-more.vue';
import tsButton from '@/components/teaset/components/ts-button.vue';
import tsSearchBar from '@/components/teaset/components/ts-search-bar.vue';
import tsBanner from '@/components/teaset/components/ts-banner.vue';
import tsLine from '@/components/teaset/components/ts-line.vue';

//全局注册
Vue.component('ts-badge', tsBadge);
Vue.component('ts-icon', tsIcon);
Vue.component('ts-tag', tsTag);
Vue.component('ts-load-more', tsLoadMore);
Vue.component('ts-button', tsButton);
Vue.component('ts-search-bar', tsSearchBar);
Vue.component('ts-banner', tsBanner);
Vue.component('ts-line', tsLine);

//以下对象需要取消，没有使用的必要性
Vue.prototype.$request = request
Vue.prototype.$store = store

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
